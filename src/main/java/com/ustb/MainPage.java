package com.ustb;

import com.ustb.bean.DirecPanel;
import com.ustb.bean.ExcelPanel;
import com.ustb.bean.ProgressPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by admin on 2018/11/27 0027.
 */
public class MainPage {
    private JFrame mainFrame;
    private JProgressBar progressBar;


    public MainPage() {
        initProgram();
    }

    private void initProgram(){
        mainFrame=new JFrame("picture excel combine");
        mainFrame.setSize(600,400);
        mainFrame.setLayout(new GridLayout(3, 2,3,3));
        mainFrame.add(new ExcelPanel(new GridBagLayout()));
        mainFrame.add(new DirecPanel(new GridBagLayout()));
        mainFrame.add(new ProgressPanel(new GridBagLayout()));


        mainFrame.setVisible(true);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent){
                System.exit(0);
            }
        });
    }

    public static void main(String[] args){
        MainPage mainPage=new MainPage();
    }
}
