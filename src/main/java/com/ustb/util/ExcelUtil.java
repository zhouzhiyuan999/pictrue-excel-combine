package com.ustb.util;

import com.ustb.bean.StudentCard;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ExcelUtil {
    private static final String EXCEL_XLS = "xls";
    private static final String EXCEL_XLSX = "xlsx";


    /**
     * 判断Excel的版本,获取Workbook
     * @param file
     * @return
     * @throws IOException
     */
    public static Workbook getWorkbok(File file) throws IOException {
        Workbook wb = null;
        FileInputStream in = new FileInputStream(file); // 文件流
        try{
            if(file.getName().endsWith(EXCEL_XLS)){  //Excel 2003
                wb = new HSSFWorkbook(in);
            }else if(file.getName().endsWith(EXCEL_XLSX)){  // Excel 2007/2010
                wb = new XSSFWorkbook(in);
            }
        }finally {
            in.close();
        }
        return wb;
    }

    /**
     * 判断文件是否是excel
     * @throws Exception
     */
    public static void checkExcelVaild(File file) throws Exception{
        if(!file.exists()){
            throw new Exception("文件不存在");
        }
        if(!(file.isFile() && (file.getName().endsWith(EXCEL_XLS) || file.getName().endsWith(EXCEL_XLSX)))){
            throw new Exception("文件不是Excel");
        }
    }


    public static String getValue(Cell cell) {
        Object obj = null;
        if(cell==null){
            return "";
        }
        switch (cell.getCellType()) {
            case BOOLEAN:
                obj = cell.getBooleanCellValue();
                break;
            case ERROR:
                obj = cell.getErrorCellValue();
                break;
            case NUMERIC:
                obj = cell.getNumericCellValue();
                break;
            case STRING:
                obj = cell.getStringCellValue();
                break;
            default:
                break;
        }
        if(obj==null){
            return "";
        }else {
            return obj.toString();
        }
    }


    public static void insertPicture(Drawing patriarch,Workbook workbook ,String filepath,Integer rowIndex,Integer colIndex,Integer width,Integer heigh){
        InputStream is=null;
        try{
            File file=new File(filepath);
            if(!file.exists()){
                is=SystemInfo.class.getResourceAsStream("/"+SystemInfo.defaultPicPath);
            }else{
                is = new FileInputStream(filepath);
            }
            byte[] bytes = IOUtils.toByteArray(is);
            int pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG);
            CreationHelper helper = workbook.getCreationHelper();
            ClientAnchor anchor = helper.createClientAnchor();
            // 图片插入坐标
            anchor.setDx1(0);
            anchor.setDy1(0);
            anchor.setDx2(255);
            anchor.setDy2(255);
            anchor.setCol1(colIndex);
            anchor.setRow1(rowIndex);
            anchor.setCol2(colIndex+width);
            anchor.setRow2(rowIndex+heigh);
            Picture pict=patriarch.createPicture(anchor,pictureIdx);
            pict.resize(0.98,0.98);

            is.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static Row createRow(Sheet sheet, Integer rowIndex) {
        Row row = null;
        if (sheet.getRow(rowIndex) != null) {
            int lastRowNo = sheet.getLastRowNum();
            sheet.shiftRows(rowIndex, lastRowNo, 1);
        }
        row = sheet.createRow(rowIndex);
//        row.setZeroHeight(false);
//        row.setHeight((short)10);
        return row;
    }

    public static List<StudentCard> getStudentInfoByCard(Sheet sheet){
        List<StudentCard> studentCards=new ArrayList<StudentCard>();
        for(int j=4;j<sheet.getLastRowNum();j+=12){
            Row row=sheet.getRow(j);
            for(int k=0;k<2;k++){
                //得到学号
                String name=ExcelUtil.getValue(sheet.getRow(j).getCell(k*13+4));
                if(name==null|| name.equals("")){
                    continue;
                }
                String number=ExcelUtil.getValue(sheet.getRow(j+2).getCell(k*13+4));
                String department=ExcelUtil.getValue(sheet.getRow(j+4).getCell(k*13+4));
                String institute=ExcelUtil.getValue(sheet.getRow(j+6).getCell(k*13+4));
                String profession=ExcelUtil.getValue(sheet.getRow(j+7).getCell(k*13+4));
                String type=ExcelUtil.getValue(sheet.getRow(j+7).getCell(k*13+10));
                studentCards.add(new StudentCard(name,number,department,institute,profession,type));
                //得到学号对应的文件
//                                    String filePath=SystemInfo.getDirectPath()+"/"+studentNo+".jpg";
//                                    cell=sheet.getRow(j-5).getCell(k*13+9);
//                                    ExcelUtil.insertPicture(drawing,workbook,filePath,j-5,k*13+9,2,7);
            }
        }
        return studentCards;
    }


    public static List<StudentCard> getStudentInfoByRecord(Sheet sheet,int start,int max){
        List<StudentCard> studentCards=new ArrayList<StudentCard>();
        for(int i=start;i<=sheet.getLastRowNum()&&i<(start+max);i++){
            String name=ExcelUtil.getValue(sheet.getRow(i).getCell(0));
            if(name==null|| name.equals("")){
                break;
            }
            String number=ExcelUtil.getValue(sheet.getRow(i).getCell(1));
            String department=ExcelUtil.getValue(sheet.getRow(i).getCell(2));
            String institute=ExcelUtil.getValue(sheet.getRow(i).getCell(3));
            String profession=ExcelUtil.getValue(sheet.getRow(i).getCell(4));
            String type=ExcelUtil.getValue(sheet.getRow(i).getCell(5));
            studentCards.add(new StudentCard(name,number,department,institute,profession,type));
        }
        return studentCards;
    }


    public static void insertStudentSheet(Workbook workbook,List<StudentCard> studentCards,Integer index){
        Sheet sheet=workbook.createSheet("Page"+(index+1));
        Drawing drawing=sheet.createDrawingPatriarch();
        for(int i=0;i<60;i++){
            Row row=sheet.createRow(i);
            for(int j=0;j<26;j++){
                row.createCell(j);
            }
        }
        //设置打印格式
        PrintSetup printSetup = sheet.getPrintSetup();
        printSetup.setPaperSize(HSSFPrintSetup.A4_PAPERSIZE);
        printSetup.setScale((short)110);
        sheet.setMargin(HSSFSheet.TopMargin,( double ) 0.0 ); // 上边距
        sheet.setMargin(HSSFSheet.BottomMargin,( double ) 0.0 );
        sheet.setMargin(HSSFSheet.LeftMargin,( double ) 0.8 );
        sheet.setMargin(HSSFSheet.RightMargin,( double ) 0.0 );


        //设置单元格格式
        sheet.setColumnWidth(0,(int)((256*0.2+184)*SystemInfo.scale));
        for(int i=0;i<2;i++){
            sheet.setColumnWidth(1+i*13,(int)((256*0.56+184)*SystemInfo.scale));
            sheet.setColumnWidth(2+i*13,(int)((256*4.33+184)*SystemInfo.scale));
            sheet.setColumnWidth(3+i*13,(int)((256*0.81+184)*SystemInfo.scale));
            sheet.setColumnWidth(4+i*13,(int)((256*17.22+184)*SystemInfo.scale));
            sheet.setColumnWidth(5+i*13,(int)((256*4.44+184)*SystemInfo.scale));
            sheet.setColumnWidth(6+i*13,(int)((256*0.88+184)*SystemInfo.scale));
            sheet.setColumnWidth(7+i*13,(int)((256*0.88+184)*SystemInfo.scale));
            sheet.setColumnWidth(8+i*13,(int)((256*0.25+184)*SystemInfo.scale));
            sheet.setColumnWidth(9+i*13,(int)((256*5.78+184)*SystemInfo.scale));
            sheet.setColumnWidth(10+i*13,(int)((256*3.22+184)*SystemInfo.scale));
            sheet.setColumnWidth(11+i*13,(int)((256*3.44+184)*SystemInfo.scale));
            sheet.setColumnWidth(12+i*13,(int)((256*0.56+184)*SystemInfo.scale));
            sheet.setColumnWidth(13+i*13,(int)((256*0.56+184)*SystemInfo.scale));
        }

        CellStyle valueStyle=workbook.createCellStyle();
        valueStyle.setBorderBottom(BorderStyle.THIN);

        CellStyle head1Style=workbook.createCellStyle();
        Font headfont1=workbook.createFont();
        headfont1.setFontName("SansSerif");
        headfont1.setFontHeightInPoints((short)(13*SystemInfo.scale));
        head1Style.setFont(headfont1);
        head1Style.setAlignment(HorizontalAlignment.CENTER);

        CellStyle head2Style=workbook.createCellStyle();
        Font headfont2=workbook.createFont();
        headfont2.setFontName("SansSerif");
        headfont2.setFontHeightInPoints((short)(18*SystemInfo.scale));
        headfont2.setBold(true);
        head2Style.setFont(headfont2);
        head2Style.setAlignment(HorizontalAlignment.CENTER);


        for(int i=0;i<studentCards.size();i++){
           studentCards.get(i).createCard(workbook,drawing,sheet,i,valueStyle,head1Style,head2Style);
        }

    }
}
