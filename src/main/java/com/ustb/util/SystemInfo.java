package com.ustb.util;

public class SystemInfo {
    private  static String excelPath="E:\\code\\programdata\\后勤\\3斋.xls";
    private static String directPath="E:\\code\\programdata\\后勤\\2018年本科新生照片";
    public static String defaultPicPath="default.jpg";
    private static Integer sumCount=0;
    private volatile static Integer currentCount=0;
    public static final double scale=0.8;
    public static String getExcelPath() {
        return excelPath;
    }

    public static void setExcelPath(String excelPath) {
        SystemInfo.excelPath = excelPath;
    }

    public static String getDirectPath() {
        return directPath;
    }

    public static void setDirectPath(String directPath) {
        SystemInfo.directPath = directPath;
    }

    public static Integer getSumCount() {
        return sumCount;
    }

    public static void setSumCount(Integer sumCount) {
        SystemInfo.sumCount = sumCount;
    }

    public static Integer getCurrentCount() {
        return currentCount;
    }

    public static void setCurrentCount(Integer currentCount) {
        SystemInfo.currentCount = currentCount;
    }
}
