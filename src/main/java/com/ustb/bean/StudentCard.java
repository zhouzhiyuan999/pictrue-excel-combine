package com.ustb.bean;

import com.ustb.util.ExcelUtil;
import com.ustb.util.SystemInfo;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;

import java.util.ArrayList;
import java.util.List;

public class StudentCard {

    private String name;
    private String number;
//    公寓
    private String department;
//    学院
    private String institue;
//    专业
    private String profesion;

//    类别
    private String type;

    private static final Integer[][] pos={
            //创建标题合并区域
            {3,3,3,4},
            {4,4,2,5},
            //创建照片合并区域
            {2,8,8,11},
            //创建标签合并区域
            {6,6,2,3},
            {7,7,2,3},
            {8,8,2,3},
            {9,9,2,3},
            {10,10,2,3},
            {10,10,8,9},
            //创建学生信息合并区域
            {6,6,4,6},
            {7,7,4,6},
            {8,8,4,6},
            {9,9,4,6},
            {10,10,4,6}
    };
    private static final String[] label={
     "姓名：","学号：","公寓：","学院：","专业：","类别："
    };

    private List<String> value;

    public StudentCard() {
    }

    public StudentCard(String name, String number, String department, String institue, String profesion, String type) {
        this.name = name;
        this.number = number;
        this.department = department;
        this.institue = institue;
        this.profesion = profesion;
        this.type = type;
        this.value=new ArrayList<>();
        value.add(name);
        value.add(number);
        value.add(department);
        value.add(institue);
        value.add(profesion);
        value.add(type);

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getInstitue() {
        return institue;
    }

    public void setInstitue(String institue) {
        this.institue = institue;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public void createCard(Workbook workbook,Drawing patriarch,Sheet sheet,Integer index,CellStyle valueStyle,CellStyle head1Style,CellStyle head2Style){
        int rowindex=index/2;
        int colindex=index%2;
        //合并单元格
        for(int i=0;i<pos.length;i++){
            sheet.addMergedRegion(new CellRangeAddress(pos[i][0]+rowindex*12,pos[i][1]+rowindex*12,pos[i][2]+colindex*13,pos[i][3]+colindex*13));
        }

        //赋值
        Cell headCell=sheet.getRow(3+rowindex*12).getCell(3+colindex*13);
        headCell.setCellValue("北京科技大学");
        headCell.setCellStyle(head1Style);

        headCell=sheet.getRow(4+rowindex*12).getCell(2+colindex*13);
        headCell.setCellValue("学生公寓住宿卡");
        headCell.setCellStyle(head2Style);

        sheet.getRow(1+rowindex*12).setHeightInPoints((int)(9*SystemInfo.scale));
        sheet.getRow(2+rowindex*12).setHeightInPoints((int)(2*SystemInfo.scale));
        sheet.getRow(3+rowindex*12).setHeightInPoints((int)(20*SystemInfo.scale));
        sheet.getRow(4+rowindex*12).setHeightInPoints((int)(27*SystemInfo.scale));
        sheet.getRow(11+rowindex*12).setHeightInPoints((int)(9*SystemInfo.scale));

        //设置外边框
        CellRangeAddress brodercell=new CellRangeAddress(1+rowindex*12,11+rowindex*12,1+colindex*13,12+colindex*13);
        RegionUtil.setBorderBottom(BorderStyle.MEDIUM, brodercell, sheet);
        RegionUtil.setBorderLeft(BorderStyle.MEDIUM, brodercell, sheet);
        RegionUtil.setBorderRight(BorderStyle.MEDIUM, brodercell, sheet);
        RegionUtil.setBorderTop(BorderStyle.MEDIUM, brodercell, sheet);

        //设置图片边框
        brodercell=new CellRangeAddress(2+rowindex*12,8+rowindex*12,8+colindex*13,11+colindex*13);
        RegionUtil.setBorderBottom(BorderStyle.THIN, brodercell, sheet);
        RegionUtil.setBorderLeft(BorderStyle.THIN, brodercell, sheet);
        RegionUtil.setBorderRight(BorderStyle.THIN, brodercell, sheet);
        RegionUtil.setBorderTop(BorderStyle.THIN, brodercell, sheet);



        for(int i=0;i<6;i++){
            Row row=sheet.getRow(pos[i+3][0]+rowindex*12);
            row.setHeightInPoints(17);
            row.getCell(pos[i+3][2]+colindex*13).setCellValue(label[i]);
            row.getCell(pos[i+3][2]+2+colindex*13).setCellValue(value.get(i));

            if(i!=5){
                for(int j=0;j<3;j++){
                    Cell cell=row.getCell(pos[i+3][2]+2+colindex*13+j);
                    cell.setCellStyle(valueStyle);
                }
            }else{
                for(int j=0;j<2;j++){
                    Cell cell=row.getCell(pos[i+3][2]+2+colindex*13+j);
                    cell.setCellStyle(valueStyle);
                }
            }

        }

        //插入图片
        ExcelUtil.insertPicture(patriarch,workbook, SystemInfo.getDirectPath()+"/"+number+".jpg",3+rowindex*12,9+colindex*13,3,5);


    }
}
