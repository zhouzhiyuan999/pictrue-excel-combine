package com.ustb.bean;

import com.ustb.util.ExcelUtil;
import com.ustb.util.SystemInfo;
import org.apache.poi.hssf.usermodel.HSSFPictureData;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.omg.Messaging.SYNC_WITH_TRANSPORT;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.ustb.util.ExcelUtil.getWorkbok;

/**
 * Created by admin on 2018/11/27 0027.
 */
public class ProgressPanel extends JPanel {
    private JProgressBar progressBar;
    private JButton startBtn;

    public ProgressPanel(LayoutManager layout) {
        super(layout);
        GridBagConstraints c=new GridBagConstraints();

        this.startBtn=new JButton("开始");
        c.gridx=0;
        c.gridy=0;
        c.insets=new Insets(0,0,20,0);
        this.startBtn.setPreferredSize(new Dimension(150,40));
        this.startBtn.addActionListener(new StartClickListenser());
        this.add(startBtn,c);


        this.progressBar=new JProgressBar();
        progressBar.setStringPainted(true);
        c.gridx=0;
        c.gridy=1;
        progressBar.setPreferredSize(new Dimension(500,20));
        this.add(progressBar,c);
    }


    public class StartClickListenser implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if(SystemInfo.getExcelPath().equals("")|| SystemInfo.getDirectPath().equals("")){
                startBtn.setText("请选择文件");
                return;
            }


            //读取excel文件
            new Thread(){
                public void run(){
                    FileOutputStream fileOut = null;

                    try{
                        File excelFile = new File(SystemInfo.getExcelPath());
                        ExcelUtil.checkExcelVaild(excelFile);
                        Workbook workbook = getWorkbok(excelFile);
                        int sheetCount = workbook.getNumberOfSheets();
                        //创建新的工作簿
                        Workbook newWorkBook=new HSSFWorkbook();


                        int start=0, max=10;
                        //更改处理方式每十个建一次
                        for(int i=0;i<sheetCount;i++){
                            int rowCount=workbook.getSheetAt(i).getLastRowNum();
                            if(rowCount==0){
                                break;
                            }
                            SystemInfo.setSumCount(rowCount);
                            for(start=1;start<=rowCount;start+=max){
                                List<StudentCard> studentCards=ExcelUtil.getStudentInfoByRecord(workbook.getSheetAt(i),start,max);
                                ExcelUtil.insertStudentSheet(newWorkBook,studentCards,start/10);
                                SystemInfo.setCurrentCount(start+max);
                                SwingUtilities.invokeLater(new Runnable() {
                                    public void run() {
                                        if(SystemInfo.getCurrentCount()<SystemInfo.getSumCount()){
                                            progressBar.setValue(((SystemInfo.getCurrentCount()+1)*100)/SystemInfo.getSumCount());
                                        }else{
                                            progressBar.setValue(100);
                                        }

                                    }
                                });
                            }

                        }
                        String fileName=excelFile.getName();
                        String filePath=excelFile.getParentFile()+"\\"+fileName.substring(0,fileName.lastIndexOf("."))+(new Date()).getTime()+".xls";
                        fileOut = new FileOutputStream(filePath);//另存文件
                        newWorkBook.write(fileOut);
                        fileOut.close();
                        JOptionPane.showMessageDialog(null,"文件生成完毕，请到路径："+filePath+" 下查看");

                    }catch (Exception ex){
                        ex.printStackTrace();
                    }


                }
            }.start();
        }

    }

}
