package com.ustb.bean;

import com.ustb.util.SystemInfo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Created by admin on 2018/11/27 0027.
 */
public class DirecPanel extends JPanel {
    private JButton fileBtn;
    private JTextField textField;
    private JLabel label;

    public DirecPanel(LayoutManager layout) {
        super(layout);
        this.setSize(600,30);
        GridBagConstraints c=new GridBagConstraints();

        this.label=new JLabel("选择照片文件目录");
        c.fill=GridBagConstraints.HORIZONTAL;
        c.gridx=0;
        c.gridy=0;
        this.label.setPreferredSize(new Dimension(120,30));
        this.add(label,c);

        this.textField=new JTextField();
        c.fill=GridBagConstraints.HORIZONTAL;
        c.gridx=1;
        c.gridy=0;
        c.insets=new Insets(0,5,0,5);
        this.textField.setPreferredSize(new Dimension(350,30));
        this.textField.setEditable(false);
        this.add(textField,c);

        this.fileBtn=new JButton("...");
        c.fill=GridBagConstraints.HORIZONTAL;
        c.gridx=2;
        c.gridy=0;
        this.fileBtn.setPreferredSize(new Dimension(50,30));
        this.fileBtn.addActionListener(new FileChooseClickListenser());
        this.add(fileBtn,c);

    }


    public class FileChooseClickListenser implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            JFileChooser jfc=new JFileChooser();
            jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY );
            jfc.showDialog(new JLabel(), "选择");
            File file=jfc.getSelectedFile();
            if(file!=null){
                DirecPanel.this.textField.setText(file.getAbsolutePath());
                SystemInfo.setDirectPath(file.getAbsolutePath());
            }

//            if(file.isDirectory()){
//                System.out.println("文件夹:"+file.getAbsolutePath());
//
//            }else if(file.isFile()){
//                System.out.println("文件:"+file.getAbsolutePath());
//            }

        }
    }


}
